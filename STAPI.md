ST API Examples
===============

This is a collection of ST API examples.  Keep in mind that there are two separate APIs: the end-user API on port 443, and the admin API on port 444, so pay close attention to which API is being shown in each example.

## Contents

Section                     | Decription
--------------------------- | ----------
[Contents](#contents)       | This table of contents
[Setup](#setup)             | Getting your server and curl client setup
. [AWS](#aws-interlude)     | Some tips on running ST in Amazon EC2
. [curl -k](#bootstrapping-away-from-curl-k) | Running curl examples more securely
[Accounts](#accounts)               | Managing accounts
. [create](#create-an-account)      | Creating a new account and user
. [display](#display-an-account)    | Displaying an account
. [update](#update-an-account)      | Updating an account
. [metadata](#hateoas)      | Metadata links and HATEOAS
. [related](#account-related-resources)| Resources related to accounts
. [user list](#list-all-users)| A trick for listing all users across all accounts
. [timestamps](#timestamps) | Timestamp representation in the API and `df`
. [certificates](#certificates)| Certificates, PGP, and SSH keys
[Files](#files)              | Managing files and directories
[Transfers](#transfers)      | Actually transfering files with the API
[Configuration](#configuration)| Configuring and managing ST through APIs
[References](#references)    | References to tools used in this document


## Setup

The examples are based on `curl`, which is in many ways the poster child of REST API documentation.  Many of the command line options are repetitive, so I recommend using a curl config file.  To get started, you will need:

* ST installed somewhere.  I use AWS EC2 to host my demo ST, so the IP address is constantly changing.  So I maintain an alias for it in my /etc/hosts file, which I update whenever I start the instance (like [this](#awshosts)).  In this document, I will call this alias *st*.
* An administrator account.  In this document, I will use the default *admin* account.  Of course you will change the password to some unique *password*.
* A sample end user account.  I will use *user*, also with a unique *password*, here.  Make sure to enable the *Allow this account to submit transfers using the Transfers RESTful API* option in the account settings.
* An understanding of how to trust the https certificates protecting the APIs (unless you are planning to use `curl -k`, which is not what security guys will do).  For this document, I will assume that you are using TLS certificates for the admind and httpd that are issued from the internal ST CA, and that you have downloaded the PEM encoded cetificate for the CA into a file `ca-crt.pem` (like [this](#curlk)).
* If we are using proper https trust mechanisms, you must also make sure the certificates you are using have a CN that matches the alias you have chosen, in my case *st* (like [this](#gencert))

### AWS Interlude

Here is a little ditty:

    aws ec2 describe-instances |
    jq -r '.Reservations[].Instances[]
    |{alias:.Tags
       |map(if .Key=="Alias" then .Value else empty end)
       |select(length>0)[0],
      PublicIpAddress}
    |select(.PublicIpAddress!=null)
    |"\(.PublicIpAddress) \(.alias)"'
    
This incantation will display the public IP addresses for any of your AWS EC2 instances that have an associated "Alias" tag configured.  Unless you are reserving an AWS IP address for your instance (for a charge), the IP address will change every time you start it.  So I use the Alias tag to update my `/etc/hosts` file.

But to actually modify `/etc/hosts`, we need to turn the output filter into a `sed` command file that will (a) delete existing entries for the aliases and (b) append the new mappings, like this:

	|"$a\\\n\(.PublicIpAddress)\t\(.alias)\n/[[:<:]]\(.alias)$/d"'

And now for the big finish (with `sudo` if you have the stomach for it):

	aws ec2 describe-instances |
	jq -r '.Reservations[].Instances[]
	|{alias:.Tags
	   |map(if .Key=="Alias" then .Value else empty end)
	   |select(length>0)[0],
	  PublicIpAddress}
	|if .PublicIpAddress then
	   "$a\\\n\(.PublicIpAddress)\t\(.alias)\n"
	 else "" end+"/[[:<:]]\(.alias)$/d"' |
	sudo sed -f /dev/stdin -i .bak /etc/hosts

Instead of filtering out null `PublicIpAddress`, we just turn them into sed deletes without a corresponding append.

Of course, this means you need the `aws` and `jq` commands installed.  For me (on my Mac) this was as easy as:

	sudo easy_install awscli
	brew install jq
	
Your mileage may vary depending on your platform, so check the [AWSCLI Guide](http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html) or [JQ download](http://stedolan.github.io/jq/download/) instructions.  P.S. if you are using a Mac and are not using [Homebrew](http://brew.sh/), well now you are.

### Bootstrapping away from `curl -k`

The `-k` option suppresses all https certificate checking in `curl`, and while its use is very common in REST API documentation, it is sort of a bad habit.  And in the end, it is not that hard a habit to avoid.

First, you need to get the CA cert your ST server is using.  There are many ways to do this:

#### Direct copy
Copy the CA cert file directly from your ST server.  You can find it in `$FILEDRIVEHOME/lib/certs/issuers/ca-crt.pem`.  For example:

	sftp -P 10022 root@st:/opt/axway/SecureTransport/lib/certs/issuers/ca-crt.pem

Here I am using the native OS ssh facility, which provides access to the underlying file system.  By default on the ST appliance image, this uses port 10022.  Unless you have done something dangerous with shared folders, you should not be able to get here through port 22.

#### openssl s_client
Get the issuer cert from ST (or any web site) by using openssl's built-in SSL test client *s_client*.  The following incantation will do the job:

	openssl s_client -showcerts -connect st:443 \
	< /dev/null 2>/dev/null |
	sed -ne '/BEGIN/,/END/H
	   ${g;s/.*\(-----BEGIN\)/\1/;p;}' > ca-crt.pem

#### ST Admin API
Get the issuer cert from the ST admin API:

	curl -sS -k -H "Content-Type: application/json" -d '
		{"type":"x509",
		"usage":"trusted",
		"name":"ca"}
	' https://admin:password@st:444/api/v1.1/certificates/export |
	 sed -ne '/BEGIN/,/END/p' > ca-crt.pem
	
Use your own values for *admin*, *password*, and the *st* host (and admin port if you have changed it from 444).

#### Making it easy

As you can see from the last example, there is a fair amount of boilerplate typing for each `curl` invocation.  And now that we have our `ca-crt.pem` file, it gets a bit worse as we replace `curl -k` with `curl --cacert ca-crt.pem`.  Curiously, the answer to this is `curl -K` (that's a big *K*).

The `-K` option allows you to move some boilerplate options into a config file.  I put my config files in the `~/.st` directory to make typing easier, and note that you will need separate config files for the admin and end user APIs.  Here is an incantation to make the admin config file:

	mkdir ~/.st
	cat | sed "s|~|$HOME|" > ~/.st/admin <<.
	silent
	show-error
	header  "Content-Type: application/json"
	user    admin:password
	cacert  ~/.st/ca-crt.pem
	.
	
Of course you need to use your own *admin* and *password*, and you need to spell out `~` explicitly for the `cacert` option, hence the little `sed` filter.  Repeat for the end user API, just changing the credentials:

	cat | sed "s|~|$HOME|" > ~/.st/api <<.
	silent
	show-error
	header  "Content-Type: application/json"
	user    user:password
	cacert  ~/.st/ca-crt.pem
	.

Now you can easily and securely query the API, for example to list the names of your user accounts:

	curl -K ~/.st/admin https://st:444/api/v1.1/accounts |
	jq -r '.accounts[].name'

#### Certificates

If that didn't work, it may be because the certificate CN does not match the hostname alias you are using (like *st*).  The `-k` option disables both path checking and name checking, so in order to use certificates properly we also have to get the name right.  You can create a matching certificate in the admin web UI of course, but in the spirit of bootstrapping from `-k`, this is more fun:

	curl -K ~/.st/admin -k -d'{"usage":"local",
	    "caPassword":"password",
	    "subject":"CN=st,C=us",
	    "name":"admindx",
	    "type":"x509",
	    "keySize":"2048",
	    "validityPeriod":365,
	    "overwrite":"true"}
	    ' https://st:444/api/v1.1/certificates

So here you have to adapt to your server:

* your CA password
* your intended subject, including your alias as the CN (you can see I am a DN minimalist)
* adjust the keySize (in bits) and validityPeriod (in days) as you see fit

Finally you have to select this new certificate Alias for your https setting (the use of the *admind* alias for the web admin and the admin API is hard coded).  It is ok to use the admind certificate for https, but you can also generate a separate one by changing the name and selecting it on the Operations&rarr;Server Control page.

#### The final step

Even with the `-K` option, there is still a certain amount of boilerplate typing to invoke curl.  One more dose of `bash` shortcutting makes this even simpler.

    api()
    {
        resource=$1;
        shift;
        if (($#==1)); then flag='-d'; else flag=''; fi;
        curl $flag "$@" -K ~/.st/admin https://st:444/api/v1.1/$resource;
        echo;
    }
    alias api=api
    
This creates a simple `api` command with 1, 2, and 3+ argument forms:

    api accounts/user1
    api certificates/export '{"id":"8a6c30ef41c1f9740141c3617c2e000b"}'
    api accounts/user1 -X PUT -d '{...}'
    
With one argument, it is a simple GET (in fact, the first argument is always the resource identifier).  With 2 arguments, a `POST` (the `-d` option) is used.  Otherwise, the remaining arguments are passed directly to `curl`.

## Accounts
### create an account

Let's start with managing accounts.  To create your first account *user1*, try:

	curl -K ~/.st/admin -X PUT -d '{
		"name":"user1",
		"type" : "user",
		"homeFolder":"/home/axway/u/user1",
		"uid" : "500",
		"gid" : "1000"
		}' https://st:444/api/v1.1/accounts/user1

and if that works, add a user *alice* to the account:

	curl -K ~/.st/admin -X PUT -d '{
		"name":"alice",
		"passwordCredentials":{
			"username":"alice",
			"password":"password"}
		}' https://st:444/api/v1.1/accounts/user1/users/alice

Note that the username and the account name can be different, but there are three places where the username appears, and they must all be the same (the "name", the "username", and the final element of the PUT URL).  Now you should be able to log into your ST server using the *alice* username and you will see the *user1* account.

These examples show the bare minimum.(Well almost&mdash;it turns out that uid and gid are optional with hard-coded defaults of 10000 each.  I prefer the uid:gid combination of 500:1000 since this corresponds to axway:axway on the standard appliance image.)

### display an account

To check your handiwork, display the account and its user(s):

	curl -K ~/.st/admin https://st:444/api/v1.1/accounts/user1
	curl -K ~/.st/admin https://st:444/api/v1.1/accounts/user1/users

This will produce output that gives you an idea of the additional attributes:

```
{
  "name" : "user1",
  "homeFolder" : "/home/axway/u/user1",
  "uid" : "500",
  "gid" : "1000",
  "disabled" : false,
  "skin" : "Default HTML Template",
  "type" : "user",
  "unlicensed" : false,
  "authByEmail" : false,
  "deliveryMethod" : "Default",
  "enrollmentTypes" : [ ],
  "transfersWebServiceAllowed" : "false",
  "metadata" : {
    "links" : {
      "users" : "https://st:444/api/v1.1/accounts/user1/users",
      "sites" : "https://st:444/api/v1.1/sites?account=user1",
      "subscriptions" : "https://st:444/api/v1.1/subscriptions?account=user1",
      "certificates" : "https://st:444/api/v1.1/certificates?account=user1",
      "transferProfiles" : "https://st:444/api/v1.1/transferProfiles?account=user1"
    }
  }
}
```
and the user(s):

```
{
  "users" : [ {
    "name" : "alice",
    "authExternal" : false,
    "locked" : false,
    "failedAuthAttempts" : 0,
    "failedAuthMaximum" : null,
    "lastFailedAuth" : null,
    "passwordCredentials" : {
      "username" : ""alice",
      "password" : null,
      "passwordDigest" : "e1NIQS0yNTZ9XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=",
      "forcePasswordChange" : false,
      "lastPasswordChange" : "Tue, 08 Oct 2013 00:06:03 +0000",
      "passwordExpiryInterval" : null
    },
    "metadata" : {
      "links" : {
        "account" : "https://st:444/api/v1.1/accounts/user1",
        "user" : "https://st:444/api/v1.1/accounts/user1/users/alice"
      }
    }
  } ]
}
```

### update an account

Updating an account or user is done with a simple post.  For example, if you have created your account in a *disabled* state while you PUT the associated user, site, and other resources, you can switch it to *enabled*:

	curl -K ~/.st/admin -d '
		{"disabled":false}' https://st:444/api/v1.1/accounts/user1

Or similarly, to update the user of the account, in this case setting the *locked* flag (note that an account with a locked user may still be enabled&mdash;scheduled transfers will continue, but the user cannot login):

	curl -K ~/.st/admin -d '
		{"locked":true}' https://st:444/api/v1.1/accounts/user1/users/alice


### HATEOAS

Notice the metadata links at the bottom of each object.  These are the HATEOAS ([Hypertext As The Engine Of Application State](http://en.wikipedia.org/wiki/HATEOAS)) links, and in general you will be able to use them to link between related objects.  From this you can see that an account has not only subordinate users, but also links to sites, subscriptions, certificates, and transferProfiles.  Unlike users, these resources are top-level and can be queried with a filter *?account=user1*.

So for example, the pure REST-afarian way to get all of the information associated with an account would be to follow all of its links.  The following incantation can do this in two `curl`s (plus one more nested in backticks):

	curl -K .st/admin https://st:444/api/v1.1/accounts/user1
	curl -K .st/admin `
		curl -K .st/admin https://st:444/api/v1.1/accounts/user1 |
		jq -r '.metadata.links|to_entries|.[].value'`

### account-related resources

But all the top-level resources follow the same conventions so this indirection is not really needed if you want to get a list of all the subscriptions for an account, say.

Resource          | All                        | For an account
----------------- | -------------------------- | ---
Sites             | /api/v1.1/sites            | ?account=*account*
Subscriptions     | /api/v1.1/subscriptions    | ?accoubt=*account*
Certificates      | /api/v1.1/certificates     | ?account=*account*
Transfer Profiles | /api/v1.1/transferProfiles | ?account=*account*
Accounts          | /api/v1.1/accounts         | /*account*
Users             | (see [below](#allusers))   | /api/v1.1/accounts/*account*/users

If the lists get long, you can page the results using the `offset` and `limit` filters, as in:

	curl -K .st/admin https://st:444/api/v1.1/accounts?offset=0\&limit=10
	
When you get to the end of the list, the result array will be empty.  For these resource types, there is no way to get a count ahead of time.

### list all users

Finally, although usernames inhabit a global namespace, the user resources are subordinate to the accounts.  Sometimes, it is useful to obtain a global list of all users, for which the following incantation may prove useful:

	curl -K .st/admin `
		curl -K .st/admin https://st:444/api/v1.1/accounts |
		jq -r '.accounts[]|select(.type=="user")|.metadata.links.users'` |
	jq -s '.[].users|select(length>0)|add'

The embedded backtick `curl` extracts all the accounts, which are then filtered through `jq` to select only user accounts (not templates), and then extract and flatten the "user" metadata links.  This list feeds the outer `curl`, which then filters through a second `jq` to splice out empty user lists (accounts without users) and then to flatten all users into a single list.

### timestamps

The ST API can be somewhat inconsistent in the way it handles timestamps.  For example, the lastPasswordChange (in the passwordCredentials of the user resource) and lastFailedAuth (in the account) attributes are formatted as RFC-822 timestamps, or in Java SimpleDateFormat terms:

	new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z")

On the other hand, the passwordExpiryInterval (in passwordCredentials) is displayed as an integer number of days, and the expirationTime (in the certificate resource) is displayed as an integer number of milliseconds since the epoch (January 1, 1970).  Inspired by `jq`, I felt that this chaos could be addressed with a date format manipulation filter.  After some digging into available options, I wound up writing my own in Java, which was best suited to handle the required formats without any supporting libraries beyond the JRE itself.

The date filter is a single class file, `df.class` (yes, it is unconventional to have lowercase class names, and Eclipse duly complained).  By placing the class file in some location (like `~/.st`) and setting the $CLASSPATH (to `~/.st`), `df` can be invoked as simply as (including the setup):

	curl -o ~/.st/df.class http://axway-samples.s3-website-us-east-1.amazonaws.com/downloads/df/df.class
	export CLASSPATH=~/.st
	java df -h

And you'll get the brief help text (you can also click [here](http://axway-samples.s3-website-us-east-1.amazonaws.com/downloads/df/df.class) and save it).  Like `jq`, its intent is to filter JSON streams, and it looks for strings that look like RFC-822 timestamps or epoch milliseconds (12 or 13 digit numbers), and processes them according to a filter.  By default, the filter prints out epoch milliseconds:

	curl -K ~/.st/admin https://st:444/api/v1.1/accounts/user1/users |
	jq '.users[].passwordCredentials.lastPasswordChange'
	>> "Tue, 08 Oct 2013 00:06:03 +0000"
	
	curl -K ~/.st/admin https://st:444/api/v1.1/accounts/user1/users |
	java df |
	jq '.users[].passwordCredentials.lastPasswordChange'
	>> 1381190763000

This makes it much easier to script timestamp manipulations.  `df` has a few additional tricks, like this one for looking for passwords nearing expiration (abbreviated output):

	curl -K ~/.st/admin https://st:444/api/v1.1/accounts/user1/users |
	java df 'now-. days'
	>> "users" : [ {
	>> …
	>>   "passwordCredentials" : {
	>> …
	>>     "lastPasswordChange" : 5,
	>>     "passwordExpiryInterval" : 30
	>> …

or using some arithmetic features in `jq`:

	curl -K ~/.st/admin https://st:444/api/v1.1/accounts/user1/users |
	java df 'now-. days' |
	jq '.users[].passwordCredentials|.passwordExpiryInterval-.lastPasswordChange'
	>> 25

The final incantation to find all users whose passwords are due to expire within the next *n* days is left as an exercise.

### certificates

To generate a new X.509 login certificate from the internal CA:

	curl -K ~/.st/admin -i -o cert.mime -d '{
		"usage":"login",
		"account":"user1",
		"caPassword":"password",
		"subject":"CN=user1,C=us",
		"type":"x509",
		"keySize":"2048",
		"validityPeriod":365,
		"password":"password"}
	' https://st:444/api/v1.1/certificates

Note that you need to include `-i` to get all the MIME headers, since the resulting `-o cert.mime` file is a multi-part MIME object whose first part is the certificate metadata in JSON, and whose second part is a binary PKCS#12 file, protected by the "password" you specified.  Note also that ST does not store the private key for a login certificate, so you are responsible for getting the private key in the right format to the right place.

To process the output file into its meaningful parts, you can use a utilty like `munpack` (from the `mpack` package, available on `apt`, `yum`, `brew`, etc.).  In my testing, I found that `munpack` left an extra CRLF on the front of the PKCS#12 output file `part2`, resulting in the following final script using `dd` to strip out the extra 2 bytes:

	munpack cert.mime
	dd if=part2 bs=1 skip=2 |
	 openssl pkcs12 -in /dev/stdin -info -passin pass:password

But perhaps a more realistic use case is where the certificate is generated by an external system, so only the public key needs to be imported and attached to the account.

Up next:

* show openssl generation and PEM import
* show ssh generation and import
* show PGP key management
* forward link to server certificate management under [devops](#devops)

## Files

Show basic file operations: upload, download, dirlist, status, rename, delete

### Listing a directory

You can retrieve directory contents by sending a GET request to the files URL followed by the directory path. The path should be relative to the user's home directory.
The response is a JSON array of objects, each one representing a file. 

A returned file object has the following properties:

  - `fileName` - the file or folder name;
  - `lastModifiedTime` - the time when the file was last modified, measured in milliseconds since the epoch (00:00:00 GMT, January 1, 1970);
  - `isDirectory` - indicates whether the file is a directory or not;
  - `isRegularFile` - indicates whether the file is a regular file or not;
  - `isSymbolicLink` - indicates whether the file is a symbolic link or not;
  - `isOther` - indicates whether the file type is other or not;
  - `group` - the group;
  - `owner` - the file owner;
  - `permissions` - the file permissions. The octal notation is used to represent the permissions.
  - `size` - the file length - in bytes.

Expected HTTP status codes:
  
  - `200 OK` - on successfull server request;
  - `403 Forbidden` - if the user is not authorized to list the target directory;
  - `404 Not Found` - if the target path is missing;
  - `500 Internal Server Error` - on any unexpected server error occurred while serving the request;

To list the contents of "Directory 1" folder located in the user home folder:

    curl -K ~/.st/api https://10.232.2.45/api/v1.0/files/Directory%201

This will produce output similar to the following one:

```    
{
  "files" : [ {
    "fileName" : "Directory 2",
    "lastModifiedTime" : 1365410220000,
    "isDirectory" : true,
    "isRegularFile" : false,
    "isSymbolicLink" : false,
    "isOther" : false,
    "group" : "1111",
    "owner" : "1111",
    "permissions" : "755"
  }, {
    "fileName" : "file 1.txt",
    "lastModifiedTime" : 1365409560000,
    "size" : 0,
    "isDirectory" : false,
    "isRegularFile" : true,
    "isSymbolicLink" : false,
    "isOther" : false,
    "group" : "1111",
    "owner" : "1111",
    "permissions" : "644"
  }, {
    "fileName" : "file 2.txt",
    "lastModifiedTime" : 1365409620000,
    "size" : 4820,
    "isDirectory" : false,
    "isRegularFile" : true,
    "isSymbolicLink" : false,
    "isOther" : false,
    "group" : "1111",
    "owner" : "1111",
    "permissions" : "644"
  } ]
}
```

There are several ways to manage the order or limit the number of the returned files:

  - use `sortBy` URL parameter to specify the sort by attribute. The possible values are `fileName`, `lastModifiedTime`, `size`
  - use `order` URL parameter to specify the sort direction. The possible values are `ASC` or `DESC`

To list "Directory 1" contents, sort the files by size in descending order:

    curl -K ~/.st/api "https://10.232.2.45/api/v1.0/files/Directory%201?sortBy=size&order=DESC"

To list all text files located in "Directory 1":

    curl -K ~/.st/api https://10.232.2.45/api/v1.0/files/Directory%201/*.txt

You can use `offset` and `limit` to get particular range of files.
You can use `showdots` to visualize hidden files.
   
To get the last modified not hidden file/folder in the "Directory 1":

    curl -K ~/.st/api "https://10.232.2.45/api/v1.0/files/Directory%201/?sortBy=lastModifiedTime&offset=0&limit=1&showdots=false"
    
### Creating a folder, a file

You can create new directory or empty file by sending a PUT request to the files URL followed by the file/directory path.
The request body should contain a file object, with at least one property:

  - `isDirectory` - boolean flag indicating whether the file is a directory
  - `isRegularFile` -  boolean flag indicating whether the file is a flat file
  
You can assign additional custom file properties to the created file/directory by adding them to the file object in the request body.
When creating new file or directory, if there are not existing parent directories in the path, they will be created as well.

Expected HTTP status codes:

  - `201 Created` - on successful creation;
  - `403 Forbidden` - if the user is not authorized to create the target file or directory;
  - `500 Internal Server Error` - on any unexpected server error occurred while serving the request;
  - `501 Not Implemented` - if neither of the isDirectory or isRegularFile properties is set to true;

To creating a new folder named "New Directory" in the user's home folder:

    curl -K ~/.st/api -X PUT
        -d '{
            "isDirectory": true,
            "fileName": "New Directory"
          }' \
      "https://10.232.2.45/api/v1.0/files/New%20Directory"

To creating a file named "Untitled File.txt" in the "New Directory" folder with custom property "Client.MyCustomCommentProperty"
  

    curl -K ~/.st/api
      -X PUT 
        -d '{
            "isRegularFile": true,
            "fileName": "Untitled File.txt",
            "Client.MyCustomCommentProperty": "my comment"
          }' 
      "https://10.232.2.45/api/v1.0/files/New%20Directory/Untitled%20File.txt"

### Rename/move file

You can rename or move file in different location by sending a POST request to the files URL followed by the current file path.
The request body should contain a file object, with property "newFilePath", specifying the new file location and any custom properties to assign/update on the file.

Expected HTTP status codes:

  - `200 OK` - on successful operation;
  - `403 Forbidden` - if the user is not authorized to access the file
  - `404 Not Found` - if the target path is missing;
  - `500 Internal Server Error` - on any unexpected server error occurred while serving the request;

To move "Untitled File.txt" from "New Directory" directory to "Directory 1", rename it to "Renamed.txt" and add a new custom property "Client.LastPath".

    curl -K ~/.st/api
      -X POST
        -d '{
            "newFilePath": "/Directory 1/Renamed.txt",
            "Client.LastPath": "/New Directory/Untitled File.txt"
          }' 
      https://10.232.2.45/api/v1.0/files/New%20Directory/Untitled%20File.txt

### Delete file/empty directory

You can delete file or empty directory by sending a DELETE request to the files URL followed by the targer file path.

Expected HTTP status codes:

  - `200 OK` - on successful operation
  - `403 Forbidden` - if the user is not authorized to access the file
  - `404 Not Found` - if the target path is missing
  - `500 Internal Server Error` - on any unexpected server error occurred while serving the request

To delete the empty "New Directory"

    curl -K ~/.st/api
      -X DELETE
      https://10.232.2.45/api/v1.0/files/New%20Directory
      
### Downloading Files

To download a file send a GET request to the files resource followed by the file path. Transfer mode can be selected by adding the `transferMode` URL parameter. Possible values: `ASCII`, `BINARY` you can select a transfer mode.

#### Simple download

To download  example2.txt file

    curl -K ~/.st/api -H "Range: bytes=0-" https://10.232.2.45/api/v1.0/files/Directory%201/example2.txt

Sample server response: 

```
HTTP/1.1 200 OK
Server: SecureTransport 5.2.1 (build: 580)
Features: CHPWD;RTCK;STCK;ASC;DNDISP
Accept-Ranges: bytes
X-Frame-Options: SAMEORIGIN
Set-Cookie: FDX=44vqdjwkgywmv0p9w9n8qeky;Path=/;Secure
Set-Cookie: loggedIn=yep
Expires: Thu, 01 Jan 1970 00:00:00 GMT
Last-Modified: Tue, 09 Apr 2013 18:01:51 GMT
Content-Length: 21
Content-MD5: P7OdQ1GqERmnT0vEWEfmyQ==
Content-Range: bytes 0-20/21
Content-Type: text/plain
Content-Disposition: attachment; filename="example2.txt"; filename*=UTF-8''example2.txt
```

The example2.txt file is downloaded and saved in the current folder.<br/>
The `Range` HTTP header is used to download a part of the remote file.<br/>
The file MD5 sum is returned in the `Content-MD5` header. The MD5 sum is returned only when the request includes the `Range` header.

#### Instructing the server for the checksum verification

A client can download a file using the `DNDISP` feature.
In this case, after the file transfer, the client has to verify the checksum and submit the verification status to the server. The server filetracking entry will be finalized accordingly (filetransfer is in progress prior to finalization). 

This feature requires a stateful session.
To login use a request similar to the following one.

    curl -K ~/.st/api -c cookie.txt -X POST https://10.232.2.45/api/v1.0/myself/
    
Subsequent requests must use the cookie.txt file.

To download a file using the `DNDISP` feature use a request similar to the following one:

    curl -c cookie.txt --cacert ~/.st/ca-crt.pem -H "Features: DNDISP" -H "Range: bytes=0-" https://10.232.2.45/api/v1.0/files/Directory%201/example2.txt
    
After the download a `HEAD` request should be send to the server.
The `Disposition` header is used to signal the checksum verification.
`automatic-action; processed` indicates a successful download, `automatic-action; error,checksum` indicates a checksum mismatch error. 

    curl  -c cookie.txt --cacert ~/.st/ca-crt.pem  -I -H "Features: DNDISP"
      -H "Disposition: automatic-action; processed"
      https://10.232.2.45/api/v1.0/files/Directory%201/example2.txt

*-I argument instructs curl to use a `HEAD` request* 

After this step, the filetracking entry should be marked as successfully finished.

Finally, we need to logout by typing:
    
    curl -b cookie.txt --cacert ~/.st/ca-crt.pem -X DELETE https://10.232.2.45/api/v1.0/myself/

## Transfers

* application framework-oriented transfers with Basic App
	* introduce sites and subscriptions
* Router App
	* introduce applications, system accounts
* API driven transfers

## Configuration

* certificates
* configurationProfiles (Default)

## References

Tool | Description
---- | -----------
[awscli](http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)| Amazon Web Services command line tool
[brew](http://brew.sh/)| Command line package manager for Mac OS X
[curl](http://curl.haxx.se)| url test tool and REST poster child ([docs](http://curl.haxx.se/docs/manpage.html))
[df](http://axway-samples.s3-website-us-east-1.amazonaws.com/downloads/df/df.class)|Date and timestamp filter
[easy_install](http://pythonhosted.org/distribute/easy_install.html) | Python package manager
[jq](http://stedolan.github.io/jq/)| Super cool lightweight JSON processor ([docs](http://stedolan.github.io/jq/manual/))
[openssl](http://www.openssl.org/)| SSL/TLS toolkit ([docs](http://www.openssl.org/docs/apps/openssl.html))
sed| Unix stream editor and world's most arcane tool (docs for [mac](https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man1/sed.1.html) or [gnu](http://www.gnu.org/software/sed/manual/sed.html), or some [incantations](http://sed.sourceforge.net/sed1line.txt))
